import Link from 'next/link';
import React from 'react'
import { useSelector } from 'react-redux'
import Change from '../components/Change'
const koka = () => {
    const count = useSelector((state)=>state.colour.value);
   
    return (
        <div>
            
            <Change />
            <h1>{count}</h1>
            <Link href="/">
                <a>Home</a>
            </Link>

        </div>
    )
}

export default koka
